SERVICE_NAME = todoapp
ENV ?= test
AWS_ROLE ?= arn:aws:iam::838080186947:role/deploy-role
APP_REGION = ap-southeast-2

COMPOSE_RUN_AWS = docker-compose run --rm aws
COMPOSE_RUN_LINT = docker-compose run --rm lint
COMPOSE_RUN_GO = docker-compose run --rm go

lint: dotenv
	$(COMPOSE_RUN_LINT) yamllint .
.PHONY: lint

validate: dotenv
	$(COMPOSE_RUN_AWS) make _validate
.PHONY: validate

test:
	$(COMPOSE_RUN_GO) make _test
.PHONY: test

auth:
	$(COMPOSE_RUN_AWS) make _auth
.PHONY:

deploy: dotenv
	$(COMPOSE_RUN_AWS) make _deploy
.PHONY: deploy

# replaces .env with DOTENV if the variable is specified
dotenv:
ifdef DOTENV
	cp -f $(DOTENV) .env
else
	$(MAKE) .env
endif

# creates .env with .env.template if it doesn't exist already
.env:
	cp -f .env.template .env

_validate: _assumeRole
	for f in $$(find . -type f -name "*.yaml" ! -name docker-compose.yaml); do \
		aws cloudformation validate-template --template-body file://$$f; \
	done

_test:
	go get -v -t -d ./...
	go test -v -race -cover ./...

_auth: _assumeRole
	$(eval token = "$(shell aws ecr get-login-password --region $(APP_REGION))")
	@echo $(token) > /opt/app/auth_token


_deploy: _assumeRole
	aws --region $(APP_REGION) cloudformation deploy \
		--template-file ./deployment/template.yaml \
		--stack-name "$(SERVICE_NAME)-$(ENV)" \
		--capabilities CAPABILITY_NAMED_IAM \
		--parameter-overrides $(shell cat deployment/test.params)

_assumeRole:
ifndef AWS_SESSION_TOKEN
	$(eval ROLE = "$(shell aws sts assume-role --role-arn "$(AWS_ROLE)" --role-session-name "deploy-assume-role" --query "Credentials.[AccessKeyId, SecretAccessKey, SessionToken]" --output text)")
	$(eval export AWS_ACCESS_KEY_ID = $(shell echo $(ROLE) | cut -f1))
	$(eval export AWS_SECRET_ACCESS_KEY = $(shell echo $(ROLE) | cut -f2))
	$(eval export AWS_SESSION_TOKEN = $(shell echo $(ROLE) | cut -f3))
endif